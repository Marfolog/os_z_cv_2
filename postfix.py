g={
   '+': lambda x,y: x+y,
   '-': lambda x, y: x-y,
   '*': lambda x, y: x*y,
   '/': lambda x, y: x//y,
   }

flip = lambda f: lambda *a: f(*reversed(a))

def eval_expr(expr,d={}):
    stack=[]
    for x in expr.split():
        if(x.isdigit()):
            stack.append(int(x))
        elif(x in "+-*/"):
            stack.append(flip(g[x])(stack.pop(),stack.pop()))
        else:
            stack.append(d[x])
    return stack.pop()

def f(x,y,o):
    return " ".join(["(",y,o,x,")"])

def to_infix(expr):
    stack=[]
    for x in expr.split():
        if(x.isdigit()):
            stack.append(x)
        else:
            stack.append(f(stack.pop(),stack.pop(),x))

    return stack.pop()

def to_postfix(expr):
    op=[]
    postfix=[]
    for x in expr.split():
        if x in "+-*/":
            op.append(x)
        elif x.isdigit():
            postfix.append(x)
        elif x==')':
            postfix.append(op.pop())
    return " ".join(postfix)
